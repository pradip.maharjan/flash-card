import React, {Component} from 'react';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Logo from './Header/Logo';
import DeckScreen from './DeckScreen';
import NewCardScreen from './NewCardScreen';
import ReviewScreen from './ReviewScreen';

import {readDecks} from './../storage/decks';
import {reducer} from '../reducers/index';
import {loadData} from './../actions/creators';

let store = createStore(reducer);

let headerOptions = {
  headerTintColor: '#fff',
  headerTitle: (props) => <Logo />,
  headerTitleStyle: {
    backgroundColor: '#FFFFFF',
    fontWeight: 'bold',
  },
};

// readDecks().then((decks) => {
//   console.log('load' + JSON.stringify(decks));
//   store.dispatch(loadData(decks));
// });

const Stack = createStackNavigator();
const Navigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="DeckScreen"
          component={DeckScreen}
          options={headerOptions}
        />
        <Stack.Screen name="CardCreation" component={NewCardScreen} />
        <Stack.Screen name="ReviewScreen" component={ReviewScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Navigator />
      </Provider>
    );
  }
}

export default App;
