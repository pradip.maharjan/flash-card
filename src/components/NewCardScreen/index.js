import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {addCard} from './../../actions/creators';
import {connect} from 'react-redux';
import Button from '../Button';
import LabeledInput from '../LabeledInput';
import NormalText from '../NormalText';
import colors from './../../styles/colors';
let deckID = '';
class NewCard extends Component {
  static navigationOptions = {title: 'Create Card'};
  static initialState = {front: '', back: '', deckId: ''};
  constructor(props) {
    super(props);
    this.state = this.initialState;
  }

  _deckID = () => {
    const {navigation, route} = this.props;
    const params = route.params;
    return params.deckID;
  };

  _handleFront = (text) => {
    console.log('front:' + text);
    this.setState({front: text});
  };

  _handleBack = (text) => {
    console.log('back:' + text);
    this.setState({back: text});
  };

  _createCard = () => {
    console.log('create card log id: ' + this._deckID());
    this.props.createCard(this.state.front, this.state.back, this._deckID());
    this.props.navigation.navigate('CardCreation', {deckID: this._deckID()});
  };

  _reviewDeck = () => {
    this.props.navigation.navigate('ReviewScreen');
  };

  _doneCreating = () => {
    this.props.navigation.navigate('DeckScreen');
  };

  render() {
    return (
      <View>
        <LabeledInput
          label="Front"
          clearOnSubmit={false}
          onEntry={this._handleFront}
          onChange={this._handleFront}
        />
        <LabeledInput
          label="Back"
          clearOnSubmit={false}
          onEntry={this._handleBack}
          onChange={this._handleBack}
        />

        <Button style={styles.createButton} onPress={this._createCard}>
          <NormalText>Create Card</NormalText>
        </Button>

        <View style={styles.buttonRow}>
          <Button style={styles.secondaryButton} onPress={this._doneCreating}>
            <NormalText>Done</NormalText>
          </Button>

          <Button style={styles.secondaryButton} onPress={this._reviewDeck}>
            <NormalText>Review Deck</NormalText>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  createButton: {backgroundColor: colors.green},
  secondaryButton: {backgroundColor: colors.blue},
  buttonRow: {flexDirection: 'row'},
});

const mapStateToProps = (state) => {
  return {decks: state.decks};
};

const mapDispatchToProps = (dispatch) => {
  return {
    createCard: (front, back, deckID) => {
      dispatch(addCard(front, back, deckID));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewCard);
