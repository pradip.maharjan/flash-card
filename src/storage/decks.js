import AsyncStorage from '@react-native-community/async-storage';
import Deck from '../data/Deck';
export const DECK_KEY = 'flashcards:decks';
import {MockDecks} from '../data/Mocks';

async function read(key, deserialzer) {
  console.log('reading...' + key + 'des ...' + JSON.stringify(deserialzer));
  try {
    let val = await AsyncStorage.getItem(key);
    if ((val = !null)) {
      let readValue = JSON.parse(val).map((serialized) => {
        return deserialzer(serialized);
      });
      return readValue;
    } else {
      console.info(`${key} not found on disk`);
      return [];
    }
  } catch (error) {
    console.warn('AsyncStorage error: ', error.message);
  }
}

async function write(key, item) {
  try {
    console.log('Writing....' + JSON.stringify(item));
    await AsyncStorage.setItem(key, JSON.stringify(item));
  } catch (error) {
    console.error('AsyncStorage error:', error.message);
  }
}
export const readDecks = () => {
  return read(DECK_KEY, Deck.fromObject);
};
export const writeDecks = (decks) => {
  console.log('save deck 2' + JSON.stringify(decks));
  return write(DECK_KEY, decks);
};
const replaceData = writeDecks(MockDecks);
