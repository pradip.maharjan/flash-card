/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import Flashcards from './src/components/Flashcards';

const App = () => {
  return <Flashcards />;
};

export default App;
